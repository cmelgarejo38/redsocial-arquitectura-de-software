package backend.serviciopublicaciones;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Publicacion {
    private final int idPublicacion;
    private final int idUsuario;
    private final String contenido;
    private final String fecha;
    private final List<Integer> interesesPublicacion;

    public Publicacion(
            int idPublicacion,
            int idUsuario,
            String contenido,
            String fecha,
            List<Integer> interesesPublicacion
    ) {
        this.idPublicacion = idPublicacion;
        this.idUsuario = idUsuario;
        this.contenido = contenido;
        this.fecha = fecha;
        this.interesesPublicacion = interesesPublicacion;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getContenido() {
        return contenido;
    }

    public String getFecha() {
        return fecha;
    }

    public List<Integer> getInteresesPublicacion() {
        return listarInteresesPublicacion();
    }
    private List<Integer> listarInteresesPublicacion() {
        File archivoInteresesPublicacion = new File("PublicacionInteres.csv");
        if (archivoInteresesPublicacion.exists() && archivoInteresesPublicacion.length() != 0L) {
            List<Integer> idsIntereses = new ArrayList();

            try {
                FileReader lectorArchivo = new FileReader(archivoInteresesPublicacion);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosInteresPublicacion = linea.split(",");
                    if (this.idPublicacion == Integer.parseInt(datosInteresPublicacion[0])) {
                        idsIntereses.add(Integer.parseInt(datosInteresPublicacion[1]));
                    }
                }
            } catch (IOException var7) {
                System.out.println(var7.getMessage());
            }

            return idsIntereses;
        } else {
            return new ArrayList();
        }
    }
}

