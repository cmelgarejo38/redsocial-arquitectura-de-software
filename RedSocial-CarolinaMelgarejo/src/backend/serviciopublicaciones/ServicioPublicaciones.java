package backend.serviciopublicaciones;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ServicioPublicaciones
{
    private List<Publicacion> listaPublicaciones;

    private final int numeroDePublicacines;
    private int idUltimaPublicacion;

    public ServicioPublicaciones(){
        this.listaPublicaciones= new ArrayList<>();
        this.numeroDePublicacines = 0;
    }
    public int agregarPublicacion(int idUsuario, String contenidoPublication) {

        ++this.idUltimaPublicacion;
        Date fechaActual = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        String fechaCadena = formateador.format(fechaActual);
        this.registrarPublicacion(this.idUltimaPublicacion, idUsuario, contenidoPublication, fechaCadena);
        return this.idUltimaPublicacion;
    }

    public Publicacion buscarPublicacion(int idPublicacion) {
        File archivoPublicaciones = new File("Publicaciones.csv");
        if (archivoPublicaciones.exists() && archivoPublicaciones.length() != 0L) {
            try {
                FileReader lectorArchivo = new FileReader(archivoPublicaciones);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosPublicacion = linea.split("\"");
                    String[] idsPublicacion = datosPublicacion[0].substring(0, datosPublicacion[0].length() - 1).split(",");
                    if (idPublicacion == Integer.parseInt(idsPublicacion[0])) {
                        List<Integer> interesesPublicacion = new ArrayList<>();
                        return new Publicacion(idPublicacion, Integer.parseInt(idsPublicacion[1]), datosPublicacion[1], datosPublicacion[2].substring(1, datosPublicacion[2].length()),interesesPublicacion );
                    }
                }
            } catch (IOException var8) {
                System.out.println(var8.getMessage());
            }

            return null;
        } else {
            return null;
        }
    }

    public List<Integer> listarPublicaciones() {
        File archivoPublicaciones = new File("Publicaciones.csv");
        if (archivoPublicaciones.exists() && archivoPublicaciones.length() != 0L) {
            List<Integer> idPublicaciones = new ArrayList();

            try {
                FileReader lectorArchivo = new FileReader(archivoPublicaciones);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosPublicacion = linea.split(",");
                    this.idUltimaPublicacion = Integer.parseInt(datosPublicacion[0]);
                    idPublicaciones.add(this.idUltimaPublicacion);
                }
            } catch (IOException var7) {
                System.out.println(var7.getMessage());
            }

            return idPublicaciones;
        } else {
            return null;
        }
    }

    public boolean usuarioTienePublicacion(int idUsuario) {
        boolean res = false;
        File archivoPublicaciones = new File("Publicaciones.csv");
        if (archivoPublicaciones.exists() && archivoPublicaciones.length() != 0L) {
            try {
                FileReader lectorArchivo = new FileReader(archivoPublicaciones);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while ((linea = lectorBuffer.readLine()) != null) {
                    String[] datosPublicacion = linea.split("\"");
                    String[] idsPublicacion = datosPublicacion[0].substring(0, datosPublicacion[0].length() - 1).split(",");
                    List<Integer> interesesPublicacion = new ArrayList<>();
                    Publicacion x = new Publicacion(Integer.parseInt(idsPublicacion[0]), Integer.parseInt(idsPublicacion[1]), datosPublicacion[1], datosPublicacion[2].substring(1, datosPublicacion[2].length()), interesesPublicacion);
                    if (x.getIdUsuario() == idUsuario) {
                        res = true;
                    }
                }

            } catch (IOException var8) {
                System.out.println(var8.getMessage());
            }
        }
        return res;
    }
    private void registrarPublicacion(int idPublicacion, int idUsuario, String contenido, String fecha) {
        try {
                File archivoPublicaciones = new File("Publicaciones.csv");
                FileWriter escritorArchivo;
                if (archivoPublicaciones.exists() && archivoPublicaciones.length() != 0L) {
                    escritorArchivo = new FileWriter(archivoPublicaciones, true);
                } else {
                    escritorArchivo = new FileWriter(archivoPublicaciones);
                }

                BufferedWriter escritorBuffer = new BufferedWriter(escritorArchivo);
                escritorBuffer.write("" + idPublicacion + "," + idUsuario + ",\"" + contenido + "\"," + fecha);
                escritorBuffer.newLine();
                escritorBuffer.close();
                escritorArchivo.close();

        } catch (IOException var8) {
            System.out.println(var8.getMessage());
        }
    }
    public void agregarInteresPublicacion(int idPublicacion, int idInteres) {
        File archivoInteresPublicaciones = new File("PublicacionInteres.csv");

        try {
            FileWriter escritorArchivo;
            if (archivoInteresPublicaciones.exists() && archivoInteresPublicaciones.length() != 0L) {
                escritorArchivo = new FileWriter(archivoInteresPublicaciones, true);
            } else {
                escritorArchivo = new FileWriter(archivoInteresPublicaciones);
            }

            BufferedWriter escritorBuffer = new BufferedWriter(escritorArchivo);
            escritorBuffer.write("" + idPublicacion + "," + idInteres);
            escritorBuffer.newLine();
            escritorBuffer.close();
            escritorArchivo.close();
        } catch (IOException var6) {
            System.out.println(var6.getMessage());
        }

    }

}
