package backend.servicioreacciones;

import backend.serviciousuarios.TipoUsuario;

import java.io.*;
import java.util.*;

public class ServicioReacciones
{
    private final String NOMBRE_ARCHIVO = "Reacciones.csv";


    public void agregarReaccion(int idPublicacion, int idUsuario, Emocion reaccion) {
        try {

                File archivoReacciones = new File("Reacciones.csv");
                FileWriter escritorArchivo;
                if (!archivoReacciones.exists()) {
                    escritorArchivo = new FileWriter(archivoReacciones);
                } else {
                    escritorArchivo = new FileWriter(archivoReacciones, true);
                }
                    BufferedWriter escritorBuffer = new BufferedWriter(escritorArchivo);
                    escritorBuffer.write( "" + idPublicacion + "," + reaccion + "," + idUsuario + "\n");
                    escritorBuffer.close();
                    escritorArchivo.close();
        } catch (IOException var7) {
            System.out.println(var7.getMessage());
        }
    }

    public Map<Emocion, Integer> listarResumenReacciones(int idPublicacion) {
        Map<Emocion,Integer> res = new HashMap<>();
        List<List<String>> datosReacciones = leerReaccionesArchivo();
        res.put(Emocion.Like,0);
        res.put(Emocion.Love,0);
        res.put(Emocion.Sad,0);
        res.put(Emocion.Happy,0);
        res.put(Emocion.Mad,0);
        res.put(Emocion.Surprise,0);
        res.put(Emocion.Care,0);
        res.put(Emocion.Indifferent,0);
        res.put(Emocion.Explain,0);

        for(int i=0; i<datosReacciones.size();i++){
            if(Integer.parseInt(datosReacciones.get(i).get(0)) == idPublicacion){
                if(datosReacciones.get(i).get(1).equals("Like")){
                    int valor = res.get(Emocion.Like);
                    valor++;
                    res.replace(Emocion.Like,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Love")){
                    int valor = res.get(Emocion.Love);
                    valor++;
                    res.replace(Emocion.Love,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Sad")){
                    int valor = res.get(Emocion.Sad);
                    valor++;
                    res.replace(Emocion.Sad,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Happy")){
                    int valor = res.get(Emocion.Happy);
                    valor++;
                    res.replace(Emocion.Happy,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Mad")){
                    int valor = res.get(Emocion.Mad);
                    valor++;
                    res.replace(Emocion.Mad,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Surprise")){
                    int valor = res.get(Emocion.Surprise);
                    valor++;
                    res.replace(Emocion.Surprise,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Care")){
                    int valor = res.get(Emocion.Care);
                    valor++;
                    res.replace(Emocion.Care,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Indifferent")){
                    int valor = res.get(Emocion.Indifferent);
                    valor++;
                    res.replace(Emocion.Indifferent,valor);
                }
                if(datosReacciones.get(i).get(1).equals("Explain")){
                    int valor = res.get(Emocion.Explain);
                    valor++;
                    res.replace(Emocion.Explain,valor);
                }
            }
        }
        return res;
    }

    public boolean tieneMasDeTresReacciones(int idPublicacion) {
        boolean res = false;
        List<List<String>> filas = leerReaccionesArchivo();
        int contador = 0;
        for(int i=0;i< filas.size();i++){
            if(Integer.parseInt(filas.get(i).get(0)) == idPublicacion ){
                contador++;
            }
        }
        if(contador>=3){
            res = true;
        }
        return res;
    }

    public boolean usuarioTieneReaccion(int idUsuario) {
        boolean res = false;
        List<List<String>> filas = leerReaccionesArchivo();
        int contador = 0;
        for(int i=0;i< filas.size();i++){
            if(Integer.parseInt(filas.get(i).get(2)) == idUsuario){
                contador++;
            }
        }
        if(contador>=1){
            res = true;
        }
        return res;
    }

    private List<List<String>> leerReaccionesArchivo(){
        List<List<String>> res = leerCSV("Reacciones.csv");
        return res;
    }
    private List<List<String>> leerCSV(String file) {
        List<String> lineas = new ArrayList<String>();
        try {
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String linea = bufferedReader.readLine();
            while (linea != null) {
                lineas.add(linea);
                linea = bufferedReader.readLine();
            }
            bufferedReader.close();
            fileReader.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        List<List<String>> res = new ArrayList<>();
        ArrayList<String> lineaDatos = new ArrayList<String>();
        String dato = "";
        char comilla = (char) 34;
        if(lineas.size() > 0) {
            for (int j = 0; j < lineas.size(); j++) {
                String datos = lineas.get(j);
                if (!datos.equals("") && !datos.equals("\n")) {
                    for (int i = 0; i < datos.length(); i++) {
                        if (datos.charAt(i) == ',') {
                            lineaDatos.add(dato);
                            dato = "";
                        } else {
                            if (datos.charAt(i) != comilla) {
                                dato = dato + datos.charAt(i);
                            } else {
                                if (datos.charAt(i) == comilla) {
                                    dato = "";
                                    i++;
                                    while (datos.charAt(i) != comilla && i < datos.length()) {
                                        dato = dato + datos.charAt(i);
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                    lineaDatos.add(dato);
                    res.add(lineaDatos);
                    dato = "";
                    lineaDatos = new ArrayList<String>();
                }
            }
        }
        return res;
    }
    private boolean verificarReaccionUsuario(int idP, int idU) {
        boolean res = false;
        List<List<String>> filas = leerReaccionesArchivo();
        int contador = 0;
        for(int i=0;i< filas.size();i++){
            if(Integer.parseInt(filas.get(i).get(0)) == idP && idU ==  Integer.parseInt(filas.get(i).get(2))){
                contador++;
            }
        }
        if(contador>=1){
            res = true;
        }
        return res;
    }

}
