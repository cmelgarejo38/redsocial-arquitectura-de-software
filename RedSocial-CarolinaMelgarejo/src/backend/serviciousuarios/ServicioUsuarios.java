
package backend.serviciousuarios;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ServicioUsuarios {
    private final String NOMBRE_ARCHIVO_USUARIOS = "Usuarios.csv";
    private final String NOMBRE_ARCHIVO_INTERES_USUARIOS = "UsuarioInteres.csv";
    private List<Integer> listaIdsUsuarios = this.listarUsuarios();

    public ServicioUsuarios() {
    }

    public int agregarUsuario(String nombre) {
        int idUsuario;
        if ((idUsuario = this.existeUsuario(nombre)) > 0) {
            return idUsuario;
        } else {
            File archivoUsuarios = new File("Usuarios.csv");

            try {
                FileWriter escritorArchivo;
                if (archivoUsuarios.exists() && archivoUsuarios.length() != 0L) {
                    escritorArchivo = new FileWriter(archivoUsuarios, true);
                    idUsuario = this.listaIdsUsuarios.size() + 1;
                } else {
                    escritorArchivo = new FileWriter(archivoUsuarios);
                    ++idUsuario;
                }

                this.listaIdsUsuarios.add(idUsuario);
                BufferedWriter escritorBuffer = new BufferedWriter(escritorArchivo);
                escritorBuffer.write(idUsuario + "," + nombre + "," + TipoUsuario.CANDIDATO);
                escritorBuffer.newLine();
                escritorBuffer.close();
                escritorArchivo.close();
            } catch (IOException var6) {
                System.out.println(var6.getMessage());
            }

            return idUsuario;
        }
    }

    public void eliminarUsuario(String nombre) {
        File archivoUsuarios = new File("Usuarios.csv");
        File archivoTemporal = new File("Temporal.csv");

        try {
            BufferedReader lectorBuffer = new BufferedReader(new FileReader(archivoUsuarios));
            BufferedWriter escritorBuffer = new BufferedWriter(new FileWriter(archivoTemporal));

            String linea;
            while((linea = lectorBuffer.readLine()) != null) {
                String[] datosUsuario = linea.split(",");
                if (!nombre.equals(datosUsuario[1])) {
                    escritorBuffer.write(linea);
                    escritorBuffer.flush();
                    escritorBuffer.newLine();
                }
            }

            lectorBuffer.close();
            escritorBuffer.close();
            archivoUsuarios.delete();
            archivoTemporal.renameTo(archivoUsuarios);
        } catch (IOException var8) {
            System.out.println(var8.getMessage());
        }

    }

    public Usuario buscarUsuario(int idUsuario) {
        File archivoUsuarios = new File("Usuarios.csv");
        if (archivoUsuarios.exists() && archivoUsuarios.length() != 0L) {
            try {
                FileReader lectorArchivo = new FileReader(archivoUsuarios);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosUsuario = linea.split(",");
                    if (idUsuario == Integer.parseInt(datosUsuario[0])) {
                        Usuario usuario = new Usuario(idUsuario, datosUsuario[1]);
                        if (datosUsuario[2].equalsIgnoreCase(TipoUsuario.USUARIO.name())) {
                            usuario.cambiarAUsuario();
                        }

                        return usuario;
                    }
                }
            } catch (IOException var8) {
                System.out.println(var8.getMessage());
            }

            return null;
        } else {
            return null;
        }
    }

    public List<Integer> listarUsuarios() {
        File archivoUsuarios = new File("Usuarios.csv");
        if (archivoUsuarios.exists() && archivoUsuarios.length() != 0L) {
            ArrayList idUsuarios = new ArrayList();

            try {
                FileReader lectorArchivo = new FileReader(archivoUsuarios);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosUsuario = linea.split(",");
                    idUsuarios.add(Integer.parseInt(datosUsuario[0]));
                }
            } catch (IOException var7) {
                System.out.println(var7.getMessage());
            }

            return idUsuarios;
        } else {
            return null;
        }
    }

    public void cambiarAUsuario(int idUsuario) {
        try {
            Scanner escaner = new Scanner(new File("Usuarios.csv"));
            StringBuffer cadenaBuffer = new StringBuffer();
            String usuarioDatosAnterior = "";

            String usuarioDatosActual;
            String contenidoArchivoUsuarios;
            String[] datosUsuario;
            for(usuarioDatosActual = ""; escaner.hasNextLine(); cadenaBuffer.append(contenidoArchivoUsuarios + System.lineSeparator())) {
                contenidoArchivoUsuarios = escaner.nextLine();
                datosUsuario = contenidoArchivoUsuarios.split(",");
                if (Integer.parseInt(datosUsuario[0]) == idUsuario) {
                    usuarioDatosAnterior = idUsuario + "," + datosUsuario[1] + "," + datosUsuario[2];
                    usuarioDatosActual = idUsuario + "," + datosUsuario[1] + "," + TipoUsuario.USUARIO;
                }
            }

            contenidoArchivoUsuarios = cadenaBuffer.toString();
            escaner.close();
            contenidoArchivoUsuarios = contenidoArchivoUsuarios.replaceAll(usuarioDatosAnterior, usuarioDatosActual);
            datosUsuario = null;

            try {
                FileWriter escritorArchivo = new FileWriter("Usuarios.csv");
                escritorArchivo.append(contenidoArchivoUsuarios);
                escritorArchivo.flush();
            } catch (IOException var9) {
                System.out.println(var9.getMessage());
            }
        } catch (FileNotFoundException var10) {
            System.out.println(var10.getMessage());
        }

    }

    public void agregarInteresUsuario(int idUsuario, int idInteres) {
        File archivoInteresUsuarios = new File("UsuarioInteres.csv");

        try {
            FileWriter escritorArchivo;
            if (archivoInteresUsuarios.exists() && archivoInteresUsuarios.length() != 0L) {
                escritorArchivo = new FileWriter(archivoInteresUsuarios, true);
            } else {
                escritorArchivo = new FileWriter(archivoInteresUsuarios);
            }

            BufferedWriter escritorBuffer = new BufferedWriter(escritorArchivo);
            escritorBuffer.write(idUsuario + "," + idInteres);
            escritorBuffer.newLine();
            escritorBuffer.close();
            escritorArchivo.close();
        } catch (IOException var6) {
            System.out.println(var6.getMessage());
        }

    }

    private int existeUsuario(String nombreUsuario) {
        File archivoUsuarios = new File("Usuarios.csv");
        if (archivoUsuarios.exists() && archivoUsuarios.length() != 0L) {
            try {
                FileReader lectorArchivo = new FileReader(archivoUsuarios);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosUsuario = linea.split(",");
                    if (nombreUsuario.equals(datosUsuario[1])) {
                        return Integer.parseInt(datosUsuario[0]);
                    }
                }

                lectorBuffer.close();
                lectorArchivo.close();
            } catch (IOException var7) {
                System.out.println(var7.getMessage());
            }

            return 0;
        } else {
            return 0;
        }
    }
}
