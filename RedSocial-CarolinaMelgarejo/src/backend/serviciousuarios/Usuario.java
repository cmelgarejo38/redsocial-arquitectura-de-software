//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package backend.serviciousuarios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Usuario {
    private int id;
    private String nombre;
    private TipoUsuario estado;
    private List<Integer> interesesUsuario;

    public Usuario(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.estado = TipoUsuario.CANDIDATO;
        this.interesesUsuario = this.listarInteresesUsuario();
    }
    public Usuario(int id, String nombre, List<Integer> interesesUsuario) {
        this.id = id;
        this.nombre = nombre;
        this.interesesUsuario = interesesUsuario;
    }
    public String getNombre() {
        return this.nombre;
    }
    public boolean esUsuario() {
        return this.estado.equals(TipoUsuario.USUARIO);
    }
    public void cambiarAUsuario() {
        this.estado = TipoUsuario.USUARIO;
    }
    public List<Integer> getInteresesUsuario() {
        return this.interesesUsuario;
    }
    private List<Integer> listarInteresesUsuario() {
        String NOMBRE_ARCHIVO_INTERES_USUARIOS = "UsuarioInteres.csv";
        List<Integer> idsIntereses = new ArrayList();
        File archivoInteresesUsuario = new File(NOMBRE_ARCHIVO_INTERES_USUARIOS);
        if (archivoInteresesUsuario.exists() && archivoInteresesUsuario.length() != 0L) {
            try {
                FileReader lectorArchivo = new FileReader(archivoInteresesUsuario);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while((linea = lectorBuffer.readLine()) != null) {
                    String[] datosInteresUsuario = linea.split(",");
                    if (this.id == Integer.parseInt(datosInteresUsuario[0])) {
                        idsIntereses.add(Integer.parseInt(datosInteresUsuario[0]));
                    }
                }
            } catch (IOException var8) {
                System.out.println(var8.getMessage());
            }

            return idsIntereses;
        } else {
            return idsIntereses;
        }
    }
}