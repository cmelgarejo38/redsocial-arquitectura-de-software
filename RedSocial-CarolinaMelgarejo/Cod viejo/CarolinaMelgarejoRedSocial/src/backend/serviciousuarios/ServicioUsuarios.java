package backend.serviciousuarios;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ServicioUsuarios
{
    private List<Usuario> listaUsuarios=new ArrayList<>();
    private String nombre;

    public ServicioUsuarios()
    {
        this.nombre=nombre+".csv";
        crearArchivo(this.nombre);
    }

    public int agregarUsuario(String nombre) {
        int res = this.existeNombre(nombre);
        String[] datos = this.leerDatosCSV();
        int id;
        if (datos[0].length() == 0) {
            id = 1;
            this.escribirDatosEnCSV("" + id + "," + nombre + "," + TipoUsuario.CANDIDATO.name());
            res = id;
        } else if (res == 0) {
            id = datos.length + 1;
            this.escribirDatosEnCSV("" + id + "," + nombre + "," + TipoUsuario.CANDIDATO.name());
            this.listaUsuarios.add(new Usuario(id, nombre));
            res = id;
        }

        return res;
    }

    public void eliminarUsuario(String nombre) {
        for (int i = 0; i < this.listaUsuarios.size(); ++i) {
            Usuario usu = (Usuario) this.listaUsuarios.get(i);
            if (usu.getNombre().equals(nombre)) {
                this.listaUsuarios.remove(i);
            }
        }

        this.actualizarDatos(this.converiEnCadena());
    }

    public Usuario buscarUsuario(int idUsuario) {
        Usuario usuario = null;

        for (int i = 0; i < this.listarUsuarios().size(); ++i) {
            int idU = (Integer) this.listarUsuarios().get(i);
            if (idU == idUsuario) {
                usuario = (Usuario) this.listaUsuarios.get(i);
            }
        }

        return usuario;
    }

    public List<Integer> listarUsuarios() {
        List<Integer> lista = new ArrayList();
        String[] datos = this.leerDatosCSV();
        if (!datos[0].equals("")) {
            String[] var3 = datos;
            int var4 = datos.length;

            for (int var5 = 0; var5 < var4; ++var5) {
                String dato = var3[var5];
                String[] cad = dato.split(",");
                lista.add(Integer.parseInt(cad[0]));
            }
        }

        return lista;
    }

    public String cambiarAUsuario(int idUsuario) {
        Usuario usuario = this.buscarUsuario(idUsuario);
        for (int i = 0; i < this.listaUsuarios.size(); ++i) {
            if (((Usuario) this.listaUsuarios.get(i)).getNombre().equals(usuario.getNombre())) {
                usuario.cambiarAUsuario();
                this.listaUsuarios.set(i, usuario);
            }
        }
        this.actualizarDatos(this.converiEnCadena());
        return null;
    }
    private void actualizarDatos(String[] nombre) {
        this.escribirDeCerroEnCSV(nombre);
    }

    private String [] converiEnCadena(){
        String [] res=new String[listaUsuarios.size()];
        List<Integer>listaIdUSU=listarUsuarios();
        int i=0;

        for (Usuario usuario: listaUsuarios) {
            String cad="";
            if(usuario.esUsuario()){
                cad=listaIdUSU.get(i)+","+usuario.getNombre()+","+TipoUsuario.USUARIO.name();
            }else{
                cad=listaIdUSU.get(i)+","+usuario.getNombre()+","+TipoUsuario.CANDIDATO.name();
            }
            res[i]=cad;
            i++;
        }
        return res;
    }

    private void crearArchivo(String nombre){
        try {
            File file = new File(nombre);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void escribirDatosEnCSV(String contenido){
        try{
            PrintWriter escritor = new PrintWriter(new FileWriter(nombre,true));
            escritor.printf(contenido + "\n");
            escritor.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void escribirDeCerroEnCSV(String [] contenido){
        try{
            PrintWriter escritor = new PrintWriter(new FileWriter(nombre));
            for (String fila: contenido){
                escritor.printf(fila + "\n");
            }
            escritor.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String[] leerDatosCSV(){
        String cont = readFile(nombre);
        String [] res=cont.split("\\n");
        if(res==null){
            return null;
        }
        return res;
    }

    private static String readFile(String filePath) {
        File file = new File(filePath);
        if(!file.isFile() || file.isDirectory()) {
            return null;
        }
        try {
            String cont = new String(Files.readAllBytes(file.toPath()));
            return cont;
        } catch(Throwable e) {
            return null;
        }
    }

    public int existeNombre(String nombre){
        File archivo = new File(this.nombre);
        Scanner entrada = null;
        String linea;
        int res = 0;
        try {
            entrada = new Scanner(archivo);
            while (entrada.hasNext()) {
                linea = entrada.nextLine();
                String [] data=linea.split(",");
                if (data[1].equals(nombre)) {
                    res = Integer.parseInt(data[0]);
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            if (entrada != null) {
                entrada.close();
            }
        }
        return res;
    }
}
