package backend.serviciousuarios;

import java.util.ArrayList;
import java.util.List;

public class Usuario {
    private final int id;
    private final String nombre;
    private TipoUsuario estado;
    private final List<Integer> interesesUsuario;

    public Usuario(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
        this.estado = TipoUsuario.CANDIDATO;
        this.interesesUsuario = new ArrayList<>();
    }

    public Usuario (int id, String nombre, List<Integer> interesesUsuario) {
        this.id = id;
        this.nombre = nombre;
        this.interesesUsuario = interesesUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public boolean esUsuario() {
        return estado == TipoUsuario.USUARIO;
    }

    public void cambiarAUsuario() {
        this.estado = TipoUsuario.USUARIO;
    }

    public List<Integer> getInteresesUsuario() {
        return interesesUsuario;
    }
}


