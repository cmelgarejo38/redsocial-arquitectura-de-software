package backend.serviciointereses;

public enum TipoInteres
{
   TECNOLOGIA, ANIMALES, COMIDA, BEBIDAS,PLANTAS,VIDEOJUEGOS, NATURALEZA, ESPACIO;
}
