package backend.serviciointereses;

public class Interes {
    private final int id;
    private final String interes;

    public Interes(int idInteres, String interes)
    {
        this.id = idInteres;
        this.interes = interes;
    }

    public String getInteres() {
        return interes;
    }
}
