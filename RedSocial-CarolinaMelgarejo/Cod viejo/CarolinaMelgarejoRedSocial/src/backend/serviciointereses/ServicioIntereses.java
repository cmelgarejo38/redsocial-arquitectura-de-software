package backend.serviciointereses;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class ServicioIntereses
{
    private List<Interes> listaIntereses = new ArrayList<Interes>();
    private int id;
    private String nombre;

    public ServicioIntereses()
    {
        this.nombre=nombre+".csv";
        crearArchivo(this.nombre);
    }

    public int crearNuevoInteres(String interes)
    {
        String[] datosInteres = this.leerDatosCSV();
        if (!this.verificarSiExisteInteres(interes)) {
            this.id = datosInteres.length + 1;
            Interes actual = new Interes(this.id, interes);
            this.listaIntereses.add(actual);
            this.escribirDatosEnCSV(this.id + "," + interes);
        } else {
            System.out.println("ya existe el interes");
        }
        return this.id;
    }

    public Interes buscarInteres ( int idInteres){
        Interes res = null;
        String[] datos = this.leerDatosCSV();
        if (datos.length != 0) {
            String[] var4 = datos;
            int var5 = datos.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String dato = var4[var6];
                String[] cad = dato.split(",");
                int idI = Integer.parseInt(cad[0]);
                if (idInteres == idI) {
                    res = new Interes(idI, cad[1]);
                }
            }
        }
        return res;
    }

    public List<Integer> listarIntereses()
    {
        List<Integer> lista = new ArrayList();
        String[] datos = this.leerDatosCSV();
        if (datos.length != 0) {
            String[] var3 = datos;
            int var4 = datos.length;
            for(int var5 = 0; var5 < var4; ++var5) {
                String dato = var3[var5];
                String[] cad = dato.split(",");
                lista.add(Integer.parseInt(cad[0]));
            }
        }
        return lista;
    }



    private boolean verificarSiExisteInteres(String nomInteres) {
        boolean res = false;
        Iterator var3 = this.listaIntereses.iterator();

        while(var3.hasNext()) {
            Interes interes = (Interes)var3.next();
            if (interes.getInteres().equals(nomInteres)) {
                res = true;
            }
        }

        return res;
    }

    private void crearArchivo(String nombre){
        try {
            File file = new File(nombre);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void escribirDatosEnCSV(String contenido){
        try{
            PrintWriter escritor = new PrintWriter(new FileWriter(nombre,true));
            escritor.printf(contenido + "\n");
            escritor.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void escribirDeCerroEnCSV(String [] contenido){
        try{
            PrintWriter escritor = new PrintWriter(new FileWriter(nombre));
            for (String fila: contenido){
                escritor.printf(fila + "\n");
            }
            escritor.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public String[] leerDatosCSV(){
        String [] res=leerDatos();
        if(res[0].equals("")){
            res=new String[0] ;
        }
        return res;
    }
    private String[] leerDatos(){
        String cont = readFile(nombre);
        return cont == null ? null : cont.split("\\r?\\n");
    }

    private static String readFile(String filePath) {
        File file = new File(filePath);
        if(!file.isFile() || file.isDirectory()) {
            return null;
        }
        try {
            String cont = new String(Files.readAllBytes(file.toPath()));
            return cont;
        } catch(Throwable e) {
            return null;
        }
    }

    public int existeDato(String dato){
        File archivo = new File(this.nombre);
        Scanner entrada = null;
        String linea;
        int res = 0;
        try {
            entrada = new Scanner(archivo);
            while (entrada.hasNext()) {
                linea = entrada.nextLine();
                String [] data=linea.split(",");
                if (linea.equals(dato)) {
                    res = Integer.parseInt(data[0]);
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            if (entrada != null) {
                entrada.close();
            }
        }
        return res;
    }
}