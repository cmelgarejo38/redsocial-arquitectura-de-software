package backend.servicioreacciones;

import java.io.*;
import java.nio.file.Files;
import java.util.*;


public class ServicioReacciones
{
    List<ListaReacciones> listaDeReacciones = new ArrayList();

    public ServicioReacciones()
    {
        this.crearArchivo("Reacciones.csv");
        this.cargarDatosLista();
    }

    public void agregarReaccion(int idPublicacion,
                                int idUsuario,
                                Emocion reaccion) {
        String nombreReaccion = reaccion.name();
        if (this.verificarReaccionUsuario(idPublicacion, idUsuario)) {
            this.eliminarReacccion(idPublicacion, idUsuario);
            this.listaDeReacciones.add(new ListaReacciones(idPublicacion, nombreReaccion, idUsuario));
            this.actualizarDatos(this.transformarListaEnArregloDeCadenas());
        } else {
            this.escribirDatosEnCSV("" + idPublicacion + "," + reaccion + "," + idUsuario);
            this.listaDeReacciones.add(new ListaReacciones(idPublicacion, nombreReaccion, idUsuario));
        }
    }

    public Map<Emocion,Integer> listarResumenReacciones(int idPublicacion) {
        Map<Emocion, Integer> map = new HashMap();

        for(int i = 0; i < Emocion.values().length; ++i) {
            int cantidad = this.contarReaccionesConIdyNombre(idPublicacion, Emocion.values()[i].name());
            Emocion actual = Emocion.values()[i];
            map.put(actual, cantidad);
        }
        return map;
    }

    public boolean tieneMasDeTresReacciones(int idPublicacion) {
        File archivoReacciones = new File("Reacciones.csv");
        if (archivoReacciones.exists() && archivoReacciones.length() != 0L) {
            int totalReacciones = 0;
            new ArrayList();

            try {
                FileReader lectorArchivo = new FileReader(archivoReacciones);
                BufferedReader lectorBuffer = new BufferedReader(lectorArchivo);

                String linea;
                while ((linea = lectorBuffer.readLine()) != null) {
                    String[] datosReaccion = linea.split(",");
                    if (idPublicacion == Integer.parseInt(datosReaccion[0])) {
                        ++totalReacciones;
                    }
                }
            } catch (IOException var9) {
                System.out.println(var9.getMessage());
            }

            return totalReacciones >= 3;
        } else {
            return false;
        }
    }

    private void eliminarReacccion(int idP, int idU) {
        for(int i = 0; i < this.listaDeReacciones.size(); ++i) {
            ListaReacciones rec = (ListaReacciones )this.listaDeReacciones.get(i);
            if (rec.getIdPublicacion() == idP && rec.getIdUsuario() == idU) {
                this.listaDeReacciones.remove(i);
            }
        }

    }

    private void actualizarDatos(String[] nombre) {
        this.escribirDeCerroEnCSV(nombre);
    }

    private void cargarDatosLista() {
        String[] datos = this.leerDatosCSV();

        if (datos.length != 0 && !datos[0].equals("")) {
            String[] var2 = datos;
            int var3 = datos.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                String dato = var2[var4];
                String[] cad = dato.split(",");
                int idP = Integer.parseInt(cad[0]);
                int idU = Integer.parseInt(cad[2]);
                this.listaDeReacciones.add(new ListaReacciones (idP, cad[1], idU));
            }
        }

    }

    private int contarReaccionesConIdyNombre(int idP, String nombre) {
        int res = 0;
        Iterator var4 = this.listaDeReacciones.iterator();

        while(var4.hasNext()) {
            ListaReacciones  reaccion = (ListaReacciones )var4.next();
            if (reaccion.getIdPublicacion() == idP && reaccion.getNombre().equals(nombre)) {
                ++res;
            }
        }

        return res;
    }

    private boolean verificarReaccionUsuario(int idP, int idU) {
        boolean res = false;
        Iterator var4 = this.listaDeReacciones.iterator();

        while(var4.hasNext()) {
            ListaReacciones  reaccion = (ListaReacciones )var4.next();
            if (reaccion.getIdPublicacion() == idP && reaccion.getIdUsuario() == idU) {
                res = true;
                break;
            }
        }

        return res;
    }

    private String[] transformarListaEnArregloDeCadenas() {
        String[] res = new String[this.listaDeReacciones.size()];
        int i = 0;

        for(Iterator var3 = this.listaDeReacciones.iterator(); var3.hasNext(); ++i) {
            ListaReacciones  reaccion = (ListaReacciones )var3.next();
            Integer var10000 = reaccion.getIdPublicacion();
            String cad = "" + var10000 + "," + reaccion.getNombre() + "," + reaccion.getIdUsuario();
            res[i] = cad;
        }

        return res;
    }

    private void crearArchivo(String nombre) {
        try {
            File file = new File(nombre);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException var3) {
            System.out.println(var3.getMessage());
        }

    }

    public void escribirDatosEnCSV(String contenido) {
        try {
            PrintWriter escritor = new PrintWriter(new FileWriter("Reacciones.csv", true));
            escritor.printf(contenido + "\n");
            escritor.close();
        } catch (IOException var3) {
            System.out.println(var3.getMessage());
        }

    }

    public void escribirDeCerroEnCSV(String[] contenido) {
        try {
            PrintWriter escritor = new PrintWriter(new FileWriter("Reacciones.csv"));
            String[] var3 = contenido;
            int var4 = contenido.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                String fila = var3[var5];
                escritor.printf(fila + "\n");
            }

            escritor.close();
        } catch (IOException var7) {
            System.out.println(var7.getMessage());
        }

    }

    public String[] leerDatosCSV() {
        String cont = readFile("Reacciones.csv");
        String[] res = cont.split("\\n");
        if (res == null) {
            return null;
        } else {
            if (res[0].equals("")) {
                res = new String[0];
            }

            return res;
        }
    }

    public static String readFile(String filePath) {
        File file = new File(filePath);
        if (file.isFile() && !file.isDirectory()) {
            try {
                String cont = new String(Files.readAllBytes(file.toPath()));
                return cont;
            } catch (Throwable var3) {
                return null;
            }
        } else {
            return null;
        }
    }

}
