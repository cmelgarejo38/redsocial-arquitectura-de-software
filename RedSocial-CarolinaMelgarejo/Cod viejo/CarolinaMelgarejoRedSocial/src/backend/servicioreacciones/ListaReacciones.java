package backend.servicioreacciones;

public class ListaReacciones
{
    private Integer idPublicacion;
    private String nombre;
    private Integer idUsuario;

    public ListaReacciones(int idP, String nombre, int idU) {
        this.idPublicacion = idP;
        this.nombre = nombre;
        this.idUsuario = idU;
    }

    public Integer getIdPublicacion() {
        return this.idPublicacion;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getIdUsuario() {
        return this.idUsuario;
    }

}
