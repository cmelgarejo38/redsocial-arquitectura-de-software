package backend.serviciopublicaciones;

public class InteresesPublicacion
{
    private int idInteres;
    private int idPublicacion;

    public InteresesPublicacion(int idInteres, int idPublicacion) {
        this.idInteres = idInteres;
        this.idPublicacion = idPublicacion;
    }

    public int getIdInteres() {
        return this.idInteres;
    }

    public int getIdPublicacion() {
        return this.idPublicacion;
    }
}
