package backend.serviciopublicaciones;

import java.util.ArrayList;
import java.util.List;

public class Publicacion
{
    private int idPublicacion;
    private int idUsuario;
    private String contenido;

    private String fecha;

    private List<Integer> interesesPublicacion;


    public Publicacion(
            int idPublicacion,
            int idUsuario,
            String contenido,
            String fecha,
            List<Integer> interesesPublicacion
    ) {
        this.idPublicacion = idPublicacion;
        this.idUsuario = idUsuario;
        this.contenido = contenido;
        this.fecha = fecha;
        this.interesesPublicacion = interesesPublicacion;
    }

    public Publicacion(
            int idPublicacion,
            int idUsuario,
            String contenido,
            String fecha
    ) {
        this.idPublicacion = idPublicacion;
        this.idUsuario = idUsuario;
        this.contenido = contenido;
        this.fecha = fecha;
        this.interesesPublicacion = new ArrayList<>();
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public String getContenido() {
        return contenido;
    }

    public String getFecha() {
        return fecha;
    }

    public List<Integer> getInteresesPublicacion()
    {
        return getInteresesPublicacion();
    }
}



