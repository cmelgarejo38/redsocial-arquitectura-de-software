package backend.serviciopublicaciones;

import java.io.*;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;


public class ServicioPublicaciones
{
    private List<Publicacion> listaPublicaciones = new ArrayList();
    private String fecha = "";
    private String nombre;

    public ServicioPublicaciones()
    {
        this.nombre=nombre+".csv";
        crearArchivo(this.nombre);
    }

    public int agregarPublicacion(int idUsuario, String contenidoPublication) {
        String[] datos = this.leerDatosCSV();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        this.fecha = "" + LocalDateTime.now().format(formatter);
        int id;
        Publicacion actual;
        int res;
        if (datos.length == 0) {
            id = 1;
            actual = new Publicacion(id, idUsuario, contenidoPublication, this.fecha);
            this.listaPublicaciones.add(actual);
            this.escribirDatosEnCSV("" + id + "," + idUsuario + ",\"" + contenidoPublication + "\"," + actual.getFecha());
            res = id;
        } else {
            id = datos.length + 1;
            actual = new Publicacion(id, idUsuario, contenidoPublication, this.fecha);
            this.listaPublicaciones.add(actual);
            this.escribirDatosEnCSV("" + id + "," + idUsuario + ",\"" + contenidoPublication + "\"," + actual.getFecha());
            res = id;
        }

        return res;
    }

    public Publicacion buscarPublicacion(int idPublicacion)
    {
        Publicacion res = null;
        String[] datos = this.leerDatosCSV();
        if (datos.length != 0) {
            String[] var4 = datos;
            int var5 = datos.length;

            for(int var6 = 0; var6 < var5; ++var6) {
                String dato = var4[var6];
                String[] cad = dato.split(",");
                int idP = Integer.parseInt(cad[0]);
                if (idP == idPublicacion) {
                    int idU = Integer.parseInt(cad[1]);
                    String contenido = this.contruirContenido(cad);
                    res = new Publicacion(idP, idU, contenido, cad[cad.length - 1]);
                }
            }
        }
        return res;
    }

    public List<Integer> listarPublicaciones()
    {
        List<Integer> lista = new ArrayList();
        String[] datos = this.leerDatosCSV();
        if (datos.length != 0) {
            String[] var3 = datos;
            int var4 = datos.length;

            for(int var5 = 0; var5 < var4; ++var5) {
                String dato = var3[var5];
                String[] cad = dato.split(",");
                lista.add(Integer.parseInt(cad[0]));
            }
        }
        Collections.reverse(lista);
        return lista;
    }

    public void agregarInteresPublicacion(int idPublicacion, int idInteres) {
        File archivoInteresPublicaciones = new File("PublicacionInteres.csv");

        try {
            FileWriter escritorArchivo;
            if (archivoInteresPublicaciones.exists() && archivoInteresPublicaciones.length() != 0L) {
                escritorArchivo = new FileWriter(archivoInteresPublicaciones, true);
            } else {
                escritorArchivo = new FileWriter(archivoInteresPublicaciones);
            }

            BufferedWriter escritorBuffer = new BufferedWriter(escritorArchivo);
            escritorBuffer.write("" + idPublicacion + "," + idInteres);
            escritorBuffer.newLine();
            escritorBuffer.close();
            escritorArchivo.close();
        } catch (IOException var6) {
            System.out.println(var6.getMessage());
        }
    }


    private void cargarDatosLista() {
        String[] datos = this.leerDatosCSV();
        if (datos.length != 0 && !datos[0].equals("")) {
            String[] var2 = datos;
            int var3 = datos.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                String dato = var2[var4];
                String[] cad = dato.split(",");
                String contenido = this.contruirContenido(cad);
                contenido = contenido.replace("\"", "");
                int idP = Integer.parseInt(cad[0]);
                int idU = Integer.parseInt(cad[1]);
                Publicacion publi = new Publicacion(idP, idU, contenido, cad[cad.length - 1]);
                this.listaPublicaciones.add(publi);
            }
        }
    }

    private String contruirContenido(String[] cad) {
        String res = "";

        for(int i = 2; i < cad.length - 1; ++i) {
            if (i == cad.length - 2) {
                res = res + cad[i];
            } else {
                res = res + cad[i] + ",";
            }
        }
        return res;
    }

    private void crearArchivo(String nombre){
        try {
            File file = new File(nombre);
            if (!file.exists()) {
                file.createNewFile();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void escribirDatosEnCSV(String contenido){
        try{
            PrintWriter escritor = new PrintWriter(new FileWriter(nombre,true));
            escritor.printf(contenido + "\n");
            escritor.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public void escribirDeCerroEnCSV(String [] contenido){
        try{
            PrintWriter escritor = new PrintWriter(new FileWriter(nombre));
            for (String fila: contenido){
                escritor.printf(fila + "\n");
            }
            escritor.close();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    public String[] leerDatosCSV(){
        String cont = readFile(nombre);
        String [] res=cont.split("\\n");
        if(res==null){
            return null;
        }
        return res;
    }

    private static String readFile(String filePath) {
        File file = new File(filePath);
        if(!file.isFile() || file.isDirectory()) {
            return null;
        }
        try {
            String cont = new String(Files.readAllBytes(file.toPath()));
            return cont;
        } catch(Throwable e) {
            return null;
        }
    }

    //nos devuelve la posicion si existe toda la fila
    public int existeDato(String dato){
        File archivo = new File(this.nombre);
        Scanner entrada = null;
        String linea;
        int res = 0;
        try {
            entrada = new Scanner(archivo);
            while (entrada.hasNext()) {
                linea = entrada.nextLine();
                String [] data=linea.split(",");
                if (linea.equals(dato)) {
                    res = Integer.parseInt(data[0]);
                    break;
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            if (entrada != null) {
                entrada.close();
            }
        }
        return res;
    }
}
