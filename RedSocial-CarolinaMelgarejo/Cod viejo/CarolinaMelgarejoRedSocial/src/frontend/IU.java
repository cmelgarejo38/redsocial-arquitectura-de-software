package frontend;

import backend.serviciointereses.ServicioIntereses;
import backend.serviciopublicaciones.Publicacion;
import backend.serviciopublicaciones.ServicioPublicaciones;
import backend.servicioreacciones.Emocion;
import backend.servicioreacciones.ServicioReacciones;
import backend.serviciousuarios.ServicioUsuarios;
import backend.serviciousuarios.Usuario;

import java.io.PrintStream;
import java.util.*;

public class IU {
    private Scanner sc;
    private ServicioUsuarios servicioUsuarios;
    private ServicioReacciones servicioReacciones;
    private ServicioPublicaciones servicioPublicaciones;
    private ServicioIntereses servicioIntereses;
    private Usuario usuario;
    private List<String> listaUsuarioConvertidos;
    private int idU;
    private int idP;

    public IU(ServicioPublicaciones
                      servicioPublicaciones,
              ServicioReacciones
                      servicioReacciones,
              ServicioUsuarios
                      servicioUsuarios, ServicioIntereses servicioIntereses) {
        this.sc = new Scanner(System.in);
        this.servicioPublicaciones = servicioPublicaciones;
        this.servicioReacciones = servicioReacciones;
        this.servicioUsuarios = servicioUsuarios;
        this.servicioIntereses = servicioIntereses;
        this.listaUsuarioConvertidos = new ArrayList();
        this.servicioIntereses = new ServicioIntereses();
         }

    public void iniciar() {
        System.out.println("=======RED SOCIAL========");
        System.out.println("1. Iniciar Sesion");
        System.out.println("2. Cancelar");
        System.out.println("==================");
        int caso;
        while((caso = this.sc.nextInt()) != 0) {
            if (caso == 1) {
                iniciarSeSion();
            } else if (caso == 2) {
                break;
            }
        }
    }
    private void iniciarSeSion() {
        System.out.println("======================");
        System.out.println("Ingrese su nombre");
        System.out.println("======================");

        this.idU = this.servicioUsuarios.agregarUsuario(this.leerEntreadaTeclado());
        this.usuario = this.servicioUsuarios.buscarUsuario(this.idU);
        String tipoCandidato;
        if (this.usuario.esUsuario()) {
            tipoCandidato = "usuario";
        } else {
            tipoCandidato = "candidato";
        }
        System.out.println("Bienvenido "+ tipoCandidato );
        if (this.usuario.esUsuario()) {
            this.menuUsuario();
        } else {
            this.menuCandidato();
        }
    }
    private void cerrarSesion() {
        System.out.println("==================");
        System.out.println("Sesion Cerrada");
        System.out.println("==================");
        this.iniciar();
    }
    private void menuUsuario() {
        if (this.verificarCantidadInteresesDeUsuario()) {
            this.agregarIntereses();
        }

        this.mostrarMuroPublicaciones();
        System.out.println("========Menu Usuario=========");
        System.out.println("1. Crear Nueva Publicacion");
        System.out.println("2. Reaccionar Publicacion");
        System.out.println("3. Cerrar Sesion");
        System.out.println("0. Finalizar");
        System.out.println("=========================");

        int caso;
        while ((caso=sc.nextInt())!=0){
            if (caso == 1) {
                this.publicarNuevaPublicacon();
                this.mostrarMuroPublicaciones();
            } else if (caso == 2) {
                this.reaccionarPublicacion();
                this.mostrarMuroPublicaciones();
            } else if (caso == 3) {
                this.cerrarSesion();
            }
        }
    }

    private void menuCandidato() {
        this.mostrarMuroPublicaciones();
        System.out.println("===========Menu Candidato===========");
        if (!this.verificarRealizoPublicacion()) {
            System.out.println("1. Crear Nueva Publicacion");
        }
        System.out.println("2. Reaccionar Publicacion");
        System.out.println("3. Cerrar Sesion");
        System.out.println("0. Terminar Ejecucion");
        System.out.println("=============================");

        int caso;
        while ((caso=sc.nextInt())!=0){
            if (caso == 1) {
                crearPublicacion();
                mostrarMuroPublicaciones();
            } else if (caso == 2) {
                reaccionarPublicacion();
                mostrarMuroPublicaciones();
            } else if (caso == 3) {
                this.cerrarSesion();
            }
        }

    }

    private void crearPublicacion() {
        System.out.println("=============");
        System.out.println("Ingrese Contenido de su publicacion: ");
        String texto = this.leerEntreadaTeclado();
        System.out.println("==================");
        this.idP = this.servicioPublicaciones.agregarPublicacion(this.idU, texto);
    }


    private void publicarNuevaPublicacon() {
        if (this.usuario.esUsuario()) {
            System.out.println("1. crear publicacion con Interes");
            System.out.println("2. crear publicacion sin Interes");
            int caso = this.sc.nextInt();
            if (caso == 1) {
                this.crearPublicacion();
                this.asociarpublicacionAInteres();
            } else if (caso == 2) {
                this.crearPublicacion();
            }
        }else{

        }
    }

    private void mostrarMuroPublicaciones() {
        List<Integer> listaP = this.servicioPublicaciones.listarPublicaciones();
        Publicacion publicacion = null;

        for(int i = 0; i < listaP.size(); ++i) {
            PrintStream var10000 = System.out;
            Object var10001 = listaP.get(i);
            var10000.println("Cantidad de Publicaciones " + var10001 );
            publicacion = this.servicioPublicaciones.buscarPublicacion((Integer)listaP.get(i));
            if (this.verificarCantidadReaccione((Integer)listaP.get(i))) {
                String nombreCandidato = this.servicioUsuarios.buscarUsuario(publicacion.getIdUsuario()).getNombre();
                if (!this.listaUsuarioConvertidos.contains(nombreCandidato)) {
                    this.listaUsuarioConvertidos.add(nombreCandidato);
                }
                this.servicioUsuarios.cambiarAUsuario(publicacion.getIdUsuario());
            }
            this.mostrarPublicacion(publicacion, (Integer)listaP.get(i));
        }
        this.mensajeCantidadosConvertidos();
    }

    private void mostrarPublicacion(Publicacion publicacion, int idP) {
        System.out.println("==================MURO===============");
        System.out.println("Usuario:" +  this.servicioUsuarios.buscarUsuario(publicacion.getIdUsuario()).getNombre() );
        System.out.println("Fecha:" +publicacion.getFecha());
        System.out.println("Contenido:" + publicacion.getContenido());
        this.mostrarInteres(idP);
        this.mostrarReacciones(idP);
        System.out.println("====================");
        System.out.println();
    }

    private String leerEntreadaTeclado() {
        String cad = "";
        cad = this.sc.nextLine();
        if (cad.equals("")) {
            for(cad = this.sc.nextLine(); cad.equals(""); cad = this.sc.nextLine()) {
                System.out.println("Ingresar texto");
            }
        }
        return cad;
    }

    private void reaccionarPublicacion() {
        System.out.println("====================");
        System.out.println("NUMERO DE PUBLICACION");
        int nP = this.sc.nextInt();
        if (!this.reacionarMismaPublicacion(nP)) {
            System.out.println("SELECCIONAR UNA REACCION");
            System.out.println("1.Like  2.Love  3.Sad  4.Happy  5.Mad  6.Surprise  7.Care  8.Indifferent  9.Explain ");
            System.out.println();
            System.out.println("====================");
            int nR = this.sc.nextInt();
            --nR;
            this.servicioReacciones.agregarReaccion(nP, this.idU, Emocion.values()[nR]);
        } else {
            System.out.println("No puedes reaccionar a tu misma publicacion");
            this.reaccionarPublicacion();
        }
    }

    public void mostrarReacciones(int idP) {
        int i = 1;
        Map<Emocion, Integer> cantidad = this.servicioReacciones.listarResumenReacciones(idP);
        Emocion[] var4 = Emocion.values();
        int var5 = var4.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            Emocion e = var4[var6];
            Integer nu = (Integer)cantidad.get(e);
            if (nu != null && nu != 0) {
                PrintStream var10000 = System.out;
                String var10001 = e.toString();
                var10000.print(var10001 + "(" + nu + ")");
                ++i;
            }
        }
        System.out.println();
    }

    private boolean verificarRealizoPublicacion() {
        boolean res = false;
        List<Integer> lista = this.servicioPublicaciones.listarPublicaciones();

        for(int i = 0; i < lista.size(); ++i) {
            Publicacion publicacion = this.servicioPublicaciones.buscarPublicacion((Integer)lista.get(i));
            if (publicacion.getIdUsuario() == this.idU) {
                res = true;
            }
        }

        return res;
    }

    private boolean verificarCantidadReaccione(int idP) {
        boolean res = false;
        int cantidad = 0;
        Map<Emocion, Integer> map = this.servicioReacciones.listarResumenReacciones(idP);

        int valor;
        for(Iterator var5 = map.keySet().iterator(); var5.hasNext(); cantidad += valor) {
            Emocion clave = (Emocion)var5.next();
            valor = (Integer)map.get(clave);
        }

        if (cantidad > 2) {
            res = true;
        }

        return res;
    }

    private void mensajeCantidadosConvertidos() {
        for(int i = 0; i < this.listaUsuarioConvertidos.size(); ++i) {
            String nombre = (String)this.listaUsuarioConvertidos.get(i);
            System.out.println("=====================================");
            System.out.println("La publicacion de: " + nombre + " tuvo mas de 3 Reacciones  " + nombre + " pasa a ser USUARIO");
            System.out.println("==========================================");
        }
    }

    private void agregarIntereses() {
        System.out.println("========INTERES==========");
        System.out.println("1. Agregar Interes");
        System.out.println("2. No agregar interes");
        boolean bandera = true;

        while(bandera) {
            int caso = this.sc.nextInt();
            if (caso == 1) {
                System.out.println("Agregar interes");
                String interes = this.leerEntreadaTeclado();
                int id = this.servicioIntereses.crearNuevoInteres(interes);
                bandera = this.verificarCantidadInteresesDeUsuario();
                if (bandera) {
                    System.out.println("========INTERES==========");
                    System.out.println("1. Agregar interes");
                    System.out.println("2. No agregar interes");
                }
            } else if (caso == 2) {
                bandera = false;
            }
        }

    }

    private void asociarpublicacionAInteres() {
        Publicacion publicacion = this.servicioPublicaciones.buscarPublicacion(this.idP);
        this.mostrarPublicacion(publicacion, this.idP);
        System.out.println("====================");
        System.out.println("SELECCIONAR INTERES");
        this.listarInteres();
        int num = this.sc.nextInt();
        this.servicioPublicaciones.agregarInteresPublicacion(num,this.idP);
    }

    private void listarInteres() {
        List<Integer> lista = this.servicioIntereses.listarIntereses();

        for(int i = 0; i < lista.size(); ++i) {
            System.out.println(i + 1 + ") " + this.servicioIntereses.buscarInteres((Integer)lista.get(i)).getInteres());
        }

        System.out.println("0. Saltar Paso");
    }

    private void mostrarInteres(int idP) {
        List<Integer> lista = this.servicioIntereses.listarIntereses();
        System.out.print("Intereses:  ");

        for(int i = 0; i < lista.size(); ++i) {
            PrintStream var10000 = System.out;
            ServicioIntereses var10001 = this.servicioIntereses;
            Object var10002 = lista.get(i);
            var10000.print(" " + var10001.buscarInteres((Integer)var10002).getInteres() + ",");
        }

        System.out.println();
    }

    private boolean verificarCantidadInteresesDeUsuario() {
        boolean res = false;
        int cantidad = this.servicioIntereses.listarIntereses().size();
        System.out.println("Cantidad de Interes  " + cantidad);
        if (cantidad < 3) {
            res = true;
        }
        return res;
    }

    private boolean reacionarMismaPublicacion(int idP) {
        boolean res = false;
        Publicacion publicacion = this.servicioPublicaciones.buscarPublicacion(idP);
        if (this.idU == publicacion.getIdUsuario()) {
            res = true;
        }
        return res;
    }
}
